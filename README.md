#  Lightning Talks | SciPy 2017
## GitLab for Research
### Schedule
Thursday July 13th, 16h, Grand Ballroom

### Short Abstract
GitLab has so much potential but isn't used as heavily as other Git repo hosting sites. Here, I'll showcase why I love it in 5 minutes and why I think more people (particularly researchers, and more particularly, those interested in reproducibility) should use it more.
